<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Lista de Nomes</title>
    </head>
    <body>
        <ul>
            {{ $url }}
            @foreach($nomes as $nome)
                <li>{{ $nome }}</li>
            @endforeach
        <ul>
    </body>
</html>
