<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class ListaController extends Controller
{
    public function index (Request $request) {

        $nomes = ['Fábio', 'Amanda'];
        $url = URL::current();

        return view('lista.index', ['nomes' => $nomes, 'url' => $url]);
    }
}
